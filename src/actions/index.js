import {
    GET_NAVES_LISTADO,
    SET_NAVE_SELECCIONADA
} from '../constants';

import NavesDataService from '../services';
export const getNavesListado = () => async (dispatch) => {
    try {
      const res =await NavesDataService.getAll(); //await TutorialDataService.getAll();
      dispatch({
        type: GET_NAVES_LISTADO,
        payload: res.data && res.data.results ? res.data.results:[],
      });
    } catch (err) {
      console.log(err);
    }
};

export const setNaveSeleccionada = (payload) => async (dispatch) => {
    try {
    let pasajeros=[];
      if(payload && payload.pilots && payload.pilots.length>0){
        pasajeros=await NavesDataService.getPilotsUrl(payload.pilots); 
      }
      dispatch({
        type: SET_NAVE_SELECCIONADA,
        payload:{item:payload,pasajeros:pasajeros}
      });
    } catch (err) {
      console.log(err);
    }
};