
import axios from 'axios';

class NavesDataService {
    getAll() {
      return axios.get("https://swapi.dev/api/starships/");
    }
    getPilotsUrl=async(array)=>{
        let requestArray=[];let response=[];
        requestArray=array.map(function(item){
            return axios.get(item);
        });
        response=await axios.all(requestArray).then(axios.spread((...responses) => {
            return(responses);
          })).catch(errors => {
            return []
          })
        if(response.length>0){
            response.forEach(e => {delete e.config;delete e.request;delete e.headers;delete e.status;delete e.statusText});
        }
        return response;
    }
  }
  
  export default new NavesDataService();
