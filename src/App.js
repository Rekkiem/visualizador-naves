import React from "react";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import image1 from './image1.png';
import './App.css';
import NavesList from './components/navesList';
import "./style.css";
const App=(props)=> {
  
    return (
      <Router>
            <Switch>
              <Route exact path={["/", "/naves"]} component={NavesList} />
            </Switch>
      </Router>
    );
  
}

export default App;
