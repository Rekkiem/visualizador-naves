import React, { useEffect , useState} from "react";
import { connect,useDispatch} from "react-redux";
import NumberFormat from 'react-number-format';
import {getNavesListado,
    setNaveSeleccionada} from '../actions';
const NavesList=(state)=>{
    const {
        naves,
        naveSeleccionada,
        pasajeros
    }=state.moduloNaves;
    const dispatch = useDispatch();
    const [listado,setListado] = useState(naves);
    const [itemSeleccionado,setItemSeleccionado]=useState(naveSeleccionada);
    const [pasajerosLocal,setPasajerosLocal]=useState(pasajeros);
    const [selectedIndex,setSelectedIndex] = useState(0);
    useEffect(() => {
        if(naves.length===0){
           dispatch(getNavesListado());
        }
      }, []);

    useEffect(() => {
        setListado(naves);
      }, [naves]); 

      useEffect(() => {
        setItemSeleccionado(naveSeleccionada);
      }, [naveSeleccionada]); 

      useEffect(() => {
        setPasajerosLocal(pasajeros);
      }, [pasajeros]); 

    function onChange(event){
        const {
            value
        }=event.target;
        const findIndex=listado.findIndex(x=>x.name===value);

        if(findIndex>=0){
            setSelectedIndex(findIndex+1);
            dispatch(setNaveSeleccionada(listado[findIndex]));
        }else{
            setSelectedIndex(0);
            dispatch(setNaveSeleccionada(null));
        }
    }

    return(
        <div style={{height: '100px !important'}}>
        <div className="main-container">
            <div className="row">
                <div className="col-12 col-xs-12 col-sm-12 col">
                    <div className="form-group ml-3 mr-3 mt-4">
                        <select className="custom-select" id="naveSeleccionada" name="naveSeleccionada" defaultValue={selectedIndex} onChange={onChange}>
                        <option id={0} name={0} key={0}>Seleccione una nave...</option>
                        {listado && listado.length>0 &&
                        listado.map(function(item,index){
                            return(
                                <option id={index+1} name={index+1} key={index+1}>{item.name}</option>
                            )
                        })
                        }
                        </select>
                    </div>
                </div>
            </div>
                {itemSeleccionado!==null ?
                    <div className="row">
                        <div className="col-12 col-xs-12 col-sm-12 text-center">
                        <div className="card opacity-3 ml-3 mr-3">
                            <div className="card-body ">
                                <div className="title-1">{itemSeleccionado.name}</div>
                                <p className="simple-text">{itemSeleccionado.model}</p>
                                <hr className="line"/>
                                <div className="title-2">Fabricante</div>
                                <p className="simple-text">{itemSeleccionado.manufacturer}</p>
                                <div className="title-2">Largo</div>
                                <p className="simple-text">{<NumberFormat value={itemSeleccionado.length} displayType={'text'} thousandSeparator={"."} decimalSeparator={","} suffix=" fts." />}</p>
                                <div className="title-2">Valor</div>
                                <p className="simple-text">{<NumberFormat value={itemSeleccionado.cost_in_credits} displayType={'text'} thousandSeparator={"."} decimalSeparator={","} suffix=" créditos"/>}</p>
                                <div className="title-2">Cantidad pasajeros</div>
                                <p className="simple-text">{<NumberFormat value={itemSeleccionado.passengers} displayType={'text'} thousandSeparator={"."} decimalSeparator={","} />}</p>
                                </div>
                        </div>
                        </div>
                       
                        <div className="col-12 col-xs-12 col-sm-12 text-center pt-3">
                            <div className="card opacity-3 ml-3 mr-3 mb-3">
                            <div className="card-body ">
                            <div className="title-1">Pasajeros</div>
                            <hr className="line"/>
                            {pasajerosLocal && pasajerosLocal.length>0 &&
                                pasajerosLocal.map(function(item,index){
                                    return(
                                        <p className="simple-text" key={index}>{item.data.name}</p>
                                    )
                                })
                            }
                            </div>
                        </div>
                        </div>
                    </div>
                    :<div className="h-100">

                    </div>    
                        
                }
            
        </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        moduloNaves: state.moduloNaves,
    };
  };
export default connect(mapStateToProps, { getNavesListado,setNaveSeleccionada })(NavesList);