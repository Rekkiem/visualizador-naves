import { combineReducers } from "redux";
import naves from "./naves";

export default combineReducers({
    moduloNaves:naves,
});