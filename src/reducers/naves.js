import{
    GET_NAVES_LISTADO,
    SET_NAVE_SELECCIONADA
} from '../constants';

const initialState = {
    naves:[],
    naveSeleccionada:null,
    pasajeros:[]
};

function navesReducer(navesState = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case GET_NAVES_LISTADO:
        return{
            ...navesState,
            naves:payload
        } 
      case SET_NAVE_SELECCIONADA:{
          return{
            ...navesState,
            naveSeleccionada:payload.item,
            pasajeros:payload.pasajeros
          }
      }
      default:
        return navesState;
    }
  };
  
  export default navesReducer;